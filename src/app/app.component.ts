import{ DataService } from './data.service';
import{User} from './user.model';
import{ resultat} from './user.model';
import { Component ,OnInit} from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
 users$: resultat;
  constructor(private dataService: DataService){}
  ngOnInit()
  {
  return this.dataService.getUsers()
  .subscribe(data=>this.users$ = data);
  }
  title = 'my-new-project';
}
