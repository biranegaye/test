import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ resultat} from './user.model';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {

 apiUrl='http://localhost/senforage/client/liste';

  constructor(private _http: HttpClient)
  {
  }
   getUsers() : Observable<resultat>
     {
     return this._http.get<resultat>(this.apiUrl);
     }
}
